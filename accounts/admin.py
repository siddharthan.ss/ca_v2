from django.contrib import admin

from .models import *

# Register your models here.
admin.site.register(CustomUser)
admin.site.register(regulation)
admin.site.register(programme)
admin.site.register(department)
admin.site.register(department_section)
admin.site.register(student)
admin.site.register(staff)
admin.site.register(batch)
admin.site.register(active_batches)
admin.site.register(PasswordReset)
