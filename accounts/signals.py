from django.contrib.auth.models import Group
from django.db.models.signals import post_save
from django.dispatch import receiver

from accounts.models import student, staff


@receiver(post_save, sender=student)
def add_student_to_group(sender, instance, **kwargs):
    student_group = Group.objects.get(name='student')
    student_group.user_set.add(instance.user)
    student_group.save()
    print('Added student ' + instance.first_name + ' to student group')



@receiver(post_save, sender=staff)
def add_staff_to_group(sender, instance, **kwargs):
    if instance.designation == 'Principal':
        principal_group = Group.objects.get(name='principal')
        principal_group.user_set.add(instance.user)
        principal_group.save()


        faculty_group = Group.objects.get(name='faculty')
        faculty_group.user_set.add(instance.user)
        faculty_group.save()


    elif instance.designation == 'Professor':
        hod_group = Group.objects.get(name='hod')
        hod_group.user_set.add(instance.user)
        hod_group.save()

        faculty_group = Group.objects.get(name='faculty')
        faculty_group.user_set.add(instance.user)
        faculty_group.save()

    elif instance.designation == 'Associate Professor' or instance.designation == 'Assistant Professor':
        faculty_group = Group.objects.get(name='faculty')
        faculty_group.user_set.add(instance.user)
        faculty_group.save()
    elif instance.designation == 'Network Admin':
        network_admnin_group = Group.objects.get(name='network_admin')
        network_admnin_group.user_set.add(instance.user)
        network_admnin_group.save()
    elif instance.designation == 'Alumni Staff':
        alumini_staff_group = Group.objects.get(name='alumni_staff')
        alumini_staff_group.user_set.add(instance.user)
        alumini_staff_group.save()

