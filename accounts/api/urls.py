from django.conf.urls import url, include
from django.contrib import admin

from accounts.api.views.profile import *
from .views.approval import *
from .views.activebatches import *
from .views.batch import *
from .views.customuser_view import *
from .views.department import *
from .views.departmentsection import *
from .views.login_util import *
from .views.password_reset import *
from .views.programme import *
from .views.regulations import *
from .views.staff import *
from .views.student import *
from .views.verify_email import *

admin.autodiscover()

urlpatterns = [
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),

    url(r'^custom_user/create/$', CustomUserCreate.as_view(), name='UserCreate'),
    url(r'^custom_user/(?P<pk>[\w-]+)/edit/$', CustomUserUpdateList.as_view(), name='UserEdit'),
    url(r'^custom_user/(?P<pk>[\w-]+)/delete/$', CustomUserDeleteList.as_view(), name='UserDelete'),
    url(r'^custom_user/(?P<pk>[\w-]+)/$', CustomUserDetailedList.as_view(), name='UserDetail'),
    url(r'^avatar/update/(?P<pk>[\w-]+)/$', AvatarUpdateView.as_view(), name='AvatarUpdate'),
    url(r'^get_avatar/(?P<pk>[\w-]+)/$', GetAvatarView.as_view(), name='AvatarView'),
    url(r'^custom_user/$', CustomUserList.as_view(), name='UserList'),

    url(r'^programme/create/$', programmeCreate.as_view(), name='programmeCreate'),
    url(r'^programme/(?P<pk>[\w-]+)/edit/$', programmeUpdateList.as_view(), name='programmeEdit'),
    url(r'^progamme/(?P<pk>[\w-]+)/delete/$', programmeDeleteList.as_view(), name='programmeDelete'),
    url(r'^programme/(?P<pk>[\w-]+)/$', programmeDetailedList.as_view(), name='programmeDetail'),
    url(r'^programme/$', programmeList.as_view(), name='programmeList'),

    url(r'^department/create/$', departmentCreate.as_view(), name='departmentCreate'),
    url(r'^department/(?P<pk>[\w-]+)/edit/$', departmentUpdateList.as_view(), name='departmentEdit'),
    url(r'^department/(?P<pk>[\w-]+)/delete/$', departmentDeleteList.as_view(), name='departmentDelete'),
    url(r'^department/(?P<pk>[\w-]+)/$', departmentDetailedList.as_view(), name='departmentDetail'),
    url(r'^department/$', departmentList.as_view(), name='departmentList'),

    url(r'^department_section/create/$', DepartmentSectionCreate.as_view(), name='DepartmentSectionCreate'),
    url(r'^department_section/(?P<pk>[\w-]+)/edit/$', DepartmentSectionUpdateList.as_view(),
        name='DepartmentSectionEdit'),
    url(r'^department_section/(?P<pk>[\w-]+)/delete/$', DepartmentSectionDeleteList.as_view(),
        name='DepartmentSectionDelete'),
    url(r'^department_section/(?P<pk>[\w-]+)/$', DepartmentSectionDetailedList.as_view(),
        name='DepartmentSectionDetail'),
    url(r'^department_section/(?P<pk>[\w-]+)/link/$', DepartmentSectionDetailedLink.as_view(),
        name='DepartmentSectionDetailLink'),
    url(r'^department_section/$', DepartmentSectionList.as_view(), name='DepartmentSectionList'),


    url(r'^regulation/create/$', RegulationCreate.as_view(), name='RegulationCreate'),
    url(r'^regulation/(?P<pk>[\w-]+)/edit/$', RegulationUpdate.as_view(),
        name='RegulationEdit'),
    url(r'^regulation/(?P<pk>[\w-]+)/delete/$', RegulationDelete.as_view(),
        name='RegulationDelete'),
    url(r'^regulation/(?P<pk>[\w-]+)/$', RegulationDetailed.as_view(),
        name='RegulationDetail'),
    url(r'^regulation/$', RegulationList.as_view(), name='RegulationList'),

    url(r'^batch/create/$', BatchCreate.as_view(), name='BatchCreate'),
    url(r'^batch/(?P<pk>[\w-]+)/edit/$', BatchUpdate.as_view(), name='BatchEdit'),
    url(r'^batch/(?P<pk>[\w-]+)/delete/$', BatchDelete.as_view(), name='BatchDelete'),
    url(r'^batch/(?P<pk>[\w-]+)/$', BatchDetailed.as_view(), name='BatchDetail'),
    url(r'^batch/(?P<pk>[\w-]+)/link/$', BatchDetailedLink.as_view(), name='BatchDetailLink'),
    url(r'^batch/$', BatchList.as_view(), name='BatchList'),

    url(r'^ActiveBatches/create/$', ActiveBatchesCreate.as_view(), name='ActiveBatchCreate'),
    url(r'^ActiveBatches/(?P<pk>[\w-]+)/edit/$', ActiveBatchesUpdate.as_view(), name='ActiveBatchEdit'),
    url(r'^ActiveBatches/(?P<pk>[\w-]+)/delete/$', ActiveBatchesDelete.as_view(), name='ActiveBatchDelete'),
    url(r'^ActiveBatches/(?P<pk>[\w-]+)/$', ActiveBatchesDetailed.as_view(), name='ActiveBatchDetail'),
    url(r'^ActiveBatches/(?P<pk>[\w-]+)/link/$', ActiveBatchesDetailedLink.as_view(), name='ActiveBatchDetailLink'),
    url(r'^ActiveBatches/$', ActiveBatchesList.as_view(), name='ActiveBatchList'),

    url(r'^staff/create/$', StaffCreate.as_view(), name='StaffCreate'),
    url(r'^staff/(?P<pk>[\w-]+)/edit/$', StaffUpdate.as_view(), name='StaffEdit'),
    url(r'^staff/(?P<pk>[\w-]+)/delete/$', StaffDelete.as_view(), name='StaffDelete'),
    url(r'^staff/(?P<pk>[\w-]+)/$', StaffDetailed.as_view(), name='StaffDetail'),
    url(r'^staff/(?P<pk>[\w-]+)/link/$', StaffDetailedLink.as_view(), name='StaffDetailLInk'),
    url(r'^staff/$', StaffList.as_view(), name='StaffList'),

    url(r'^student/create/$', StudentCreate.as_view(), name='StudentCreate'),
    url(r'^student/(?P<pk>[\w-]+)/edit/$', StudentUpdate.as_view(), name='StudentEdit'),
    url(r'^student/(?P<pk>[\w-]+)/delete/$', StudentDelete.as_view(), name='StudentDelete'),
    url(r'^student/(?P<pk>[\w-]+)/$', StudentDetailed.as_view(), name='StudentDetail'),
    url(r'^student/(?P<pk>[\w-]+)/link/$', StudentDetailedLink.as_view(), name='StudentDetailLink'),
    url(r'^student/$', StudentList.as_view(), name='StudentList'),

    url(r'^verify_email/$', VerifyEmailView.as_view(), name='VerifyEmail'),
    url(r'^resend_verification_link/$', ResendVerificationLink.as_view(), name='ResendVerificationLink'),

    url(r'^password_reset/$', PasswordResetView.as_view(), name='PasswordReset'),
    url(r'^password_reset_verify/$', PasswordResetVerification.as_view(), name='PasswordResetVerify'),
    url(r'^password_reset_confirm/$', PasswordResetConfirmation.as_view(), name='PasswordResetConfirm'),

    url(r'^login_util/$', LoginHandler.as_view(), name='login_util'),

    url(r'^self/$', SelfDetail.as_view(), name='self_Detail'),

    url(r'^get_staffs_to_approve/$', GetStaffToApprove.as_view(), name='get_staffs_to_approve'),
    url(r'^approve_disapprove_users/(?P<pk>[\w-]+)/$', ApporveDisapproveUser.as_view(), name='approve_disapprove_users'),


    url(r'^get_my_profile/$', GetMyProfile.as_view(), name='get_my_profile'),
    url(r'^get_user_profile/(?P<pk>[\w-]+)/$', GetUserProfile.as_view(), name='get_user_profile'),

]
