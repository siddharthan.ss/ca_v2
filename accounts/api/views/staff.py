from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from ..serializers import *

'''
api/accounts/CustomUser/
'''


class StaffList(ListAPIView):
    serializer_class = StaffSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = staff.objects.all()
        search_type = 'icontains'
        c = []
        for f in staff._meta.get_fields():
            a = str(f)
            if 'accounts.staff' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


class StaffCreate(CreateAPIView):
    queryset = staff.objects.all()
    serializer_class = StaffSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class StaffDetailed(RetrieveAPIView):
    queryset = staff.objects.all()
    serializer_class = StaffSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class StaffDetailedLink(RetrieveAPIView):
    queryset = staff.objects.all()
    serializer_class = StaffLinkSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class StaffUpdate(RetrieveUpdateAPIView):
    queryset = staff.objects.all()
    serializer_class = StaffSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class StaffDelete(DestroyAPIView):
    queryset = staff.objects.all()
    serializer_class = StaffSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
