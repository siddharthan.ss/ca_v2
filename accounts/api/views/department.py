from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from ..serializers import *

'''
api/accounts/CustomUser/
'''


class departmentList(ListAPIView):
    serializer_class = departmentSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = ['acronym']

    def get_queryset(self, *args, **kwargs):
        queryset_list = department.objects.all()

        search_type = 'iexact'
        c = []
        for f in department._meta.get_fields():
            a = str(f)
            if 'accounts.department' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


class departmentCreate(CreateAPIView):
    queryset = department.objects.all()
    serializer_class = departmentSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class departmentDetailedList(RetrieveAPIView):
    queryset = department.objects.all()
    serializer_class = departmentSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class departmentUpdateList(RetrieveUpdateAPIView):
    queryset = department.objects.all()
    serializer_class = departmentSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class departmentDeleteList(DestroyAPIView):
    queryset = department.objects.all()
    serializer_class = departmentSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
