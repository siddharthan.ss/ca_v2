from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from ..serializers import *

'''
api/accounts/CustomUser/
'''


class BatchList(ListAPIView):
    serializer_class = BatchSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = batch.objects.all()
        search_type = 'icontains'
        c = []
        for f in batch._meta.get_fields():
            a = str(f)
            if 'accounts.batch' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


class BatchCreate(CreateAPIView):
    queryset = batch.objects.all()
    serializer_class = BatchSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class BatchDetailed(RetrieveAPIView):
    queryset = batch.objects.all()
    serializer_class = BatchSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class BatchDetailedLink(RetrieveAPIView):
    queryset = batch.objects.all()
    serializer_class = BatchLinkSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class BatchUpdate(RetrieveUpdateAPIView):
    queryset = batch.objects.all()
    serializer_class = BatchSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class BatchDelete(DestroyAPIView):
    queryset = batch.objects.all()
    serializer_class = BatchSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
