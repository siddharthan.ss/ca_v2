from rest_framework import permissions
from rest_framework.generics import UpdateAPIView
from rest_framework.mixins import UpdateModelMixin
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.models import CustomUser, staff
from accounts.api.serializers import ApprovalListSerializer, ApproveDisapproveSerializer
from common.utility.custom_permissions import IsStaff


def get_eligible_staff_accounts_to_approve(staff_instance):

    final_set_of_instances = None
    if staff_instance.user.groups.filter(name='principal').exists():
        list_of_hods = staff.objects.filter(
            designation='Professor',
            # user__is_approved=False,
            user__is_verified=True,
            user__has_filled_data=True
        )
        if final_set_of_instances:
            final_set_of_instances = final_set_of_instances | list_of_hods
        else:
            final_set_of_instances = list_of_hods
    if staff_instance.user.groups.filter(name='hod').exists():
        list_of_staffs_associates = staff.objects.filter(
            department=staff_instance.department,
            designation='Associate Professor',
            # user__is_approved=False,
            user__is_verified=True,
            user__has_filled_data=True
        )
        if final_set_of_instances:
            final_set_of_instances = final_set_of_instances | list_of_staffs_associates
        else:
            final_set_of_instances = list_of_staffs_associates
        list_of_staffs_assistants = staff.objects.filter(
            department=staff_instance.department,
            designation='Assistant Professor',
            # user__is_approved=False,
            user__is_verified=True,
            user__has_filled_data=True
        )
        if final_set_of_instances:
            final_set_of_instances = final_set_of_instances | list_of_staffs_assistants
        else:
            final_set_of_instances = list_of_staffs_assistants
        if staff_instance.department.acronym == 'CSE':
            list_of_staffs_network_admins = staff.objects.filter(
                designation='Network Admin',
                # user__is_approved=False,
                user__is_verified=True,
                user__has_filled_data=True
            )
            if final_set_of_instances:
                final_set_of_instances = final_set_of_instances | list_of_staffs_network_admins
            else:
                final_set_of_instances = list_of_staffs_network_admins

    return final_set_of_instances

class GetStaffToApprove(APIView):
    # todo add staff only permission
    permission_classes = [IsStaff]

    def get(self, request):
        staff_instance = staff.objects.get(user = request.user)
        to_be_approved_instances = get_eligible_staff_accounts_to_approve(staff_instance)
        print(to_be_approved_instances.count())
        for entry in to_be_approved_instances:
            print(entry)
        if to_be_approved_instances:
            serialized_data = ApprovalListSerializer(to_be_approved_instances,many=True).data
        else:
            serialized_data = None
        return Response(serialized_data,status=200)


class canApproveDeapprove(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.groups.filter(name='faculty').exists()

    def has_object_permission(self, request, view, obj):
        staff_instance = staff.objects.get(user=request.user)
        eligible_staffs_to_approve = get_eligible_staff_accounts_to_approve(staff_instance)
        print(eligible_staffs_to_approve)
        # print(eligible_staffs_to_approve.values_list('user'))
        # print(obj)
        to_approved_staff_intance = staff.objects.get(user=obj)
        print(to_approved_staff_intance)
        if to_approved_staff_intance in eligible_staffs_to_approve:
            print('inside')
            return True


class ApporveDisapproveUser(UpdateAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = ApproveDisapproveSerializer
    permission_classes = [canApproveDeapprove]
