import hashlib

from django.utils.crypto import random
from django.utils import timezone
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.api.serializers import VerificationLinkSerializer
from accounts.models import CustomUser
from common.response_handler import response_handler
from common.utility.email_utility import send_verification_mail


class VerifyEmailView(APIView):

    def post(self, request):
        serialized_data = VerificationLinkSerializer(data=request.data)
        if serialized_data.is_valid():
            print(vars(serialized_data.data))
            verification_key = serialized_data.validated_data.get("verification_key")
            print(verification_key)

            custom_user_query = CustomUser.objects.filter(activation_key=verification_key)
            if custom_user_query.exists():
                custom_user_instance = custom_user_query.get()
                if custom_user_instance.is_verified == True:
                    response_data = response_handler(
                        'EMAIL_ALREADY_VERIFIED',
                        'Email verification already done',
                        'Your Email ID has been already verified'
                    )
                    return Response(data=response_data, status=200)
                if timezone.now() < custom_user_instance.key_expires:
                    custom_user_instance.is_verified = True
                    custom_user_instance.save()
                    response_data = response_handler(
                        'EMAIL_VERIFIED',
                        'Email verification successful',
                        'Your Email ID has been successfully verified'
                    )
                    return Response(data=response_data,status=200)
                else:
                    response_data = response_handler(
                        'EMAIL_LINK_EXPIRED',
                        'Email verification link expired',
                        'link provided is expired'
                    )
                    return Response(data=response_data, status=400)
            else:
                response_data = response_handler(
                    'BAD_VERIFICATION_LINK',
                    'Link provided does not exist',
                    'Link seems to be not associated with any of the account on gctportal'
                )
                return Response(data=response_data, status=400)

class ResendVerificationLink(APIView):

    permission_classes = [IsAuthenticated]

    def get(self, request):
        if request.user.is_verified == False:
            random_number_string = str(random.random())
            random_number_string = random_number_string.encode('utf-8')
            salt = hashlib.sha1(random_number_string).hexdigest()[:5]
            salt = salt.encode('utf-8')
            usernamesalt = request.user.email
            usernamesalt = usernamesalt.encode('utf8')

            key = hashlib.sha1(salt + usernamesalt)
            key = key.hexdigest()
            activation_key = key
            key_expires = timezone.now() + timezone.timedelta(days=2)

            customuser_instance = CustomUser.objects.get(pk=request.user.pk)
            customuser_instance.activation_key = activation_key
            customuser_instance.key_expires = key_expires
            customuser_instance.save()

            send_verification_mail(customuser_instance)

            response_data = response_handler(
                'VERIFICATION_LINK_RESENT',
                'Email link has been re-sent',
                'An email has been sent to your regisitered mail id'
            )
            return Response(data=response_data, status=200)
        else:
            response_data = response_handler(
                'EMAIL_ALREADY_VERIFIED',
                'Email verification already done',
                'Your Email ID has been already verified'
            )
            return Response(data=response_data, status=400)