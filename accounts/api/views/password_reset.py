import hashlib

from django.utils import timezone
from django.utils.crypto import random
from rest_framework.response import Response
from rest_framework.views import APIView

from common.response_handler import response_handler
from common.utility.email_utility import send_email
from ..serializers import *


class PasswordResetView(APIView):
    def post(self, request):
        serialized_data = PasswordResetSerializer(data=request.data)
        if serialized_data.is_valid():
            email_id = serialized_data.data
            email_id = email_id['email']
            try:
                qs = CustomUser.objects.get(email=email_id)
                print(email_id)
                print('exists')
            except:
                print('matching query does not exist')
                response_data = response_handler(
                    'EMAIL_DOES_NOT_EXIST',
                    'email does not exist',
                    'Check the spelling or enter another email'
                )
                return Response(response_data, status=200)

            random_number_string = str(random.random())
            print(random_number_string)
            random_number_string = random_number_string.encode('utf-8')
            print(random_number_string)
            salt = hashlib.sha1(random_number_string).hexdigest()[:5]
            salt = salt.encode('utf-8')

            email_id_salt = email_id.encode('utf8')
            email_id_salt = hashlib.sha1(email_id_salt).hexdigest()
            email_id_salt = email_id_salt.encode('utf-8')
            key = hashlib.sha1(salt + email_id_salt)
            key = key.hexdigest()
            reset_key = key
            print(reset_key)
            key_expires = timezone.now() + timezone.timedelta(days=2)
            print(key_expires)
            from accounts.models import PasswordReset
            obj = PasswordReset.objects.create(email=qs, reset_key=reset_key, key_expires=key_expires,
                                               is_valid=True)
            send_email(obj)
            response_data = response_handler(
                'EMAIL_SENT',
                'email has been sent successfully',
                'password reset link has been sent to your mail'
            )
            return Response(response_data, status=200)

'''
{
"email":"gct@gct.ac.in"
}
'''


class PasswordResetVerification(APIView):
    def post(self, request):
        serialized_data = PasswordResetVerifySerializer(data=request.data)
        if serialized_data.is_valid():
            temp_data = serialized_data.data
            reset_key = temp_data['reset_key']
            try:
                qs = PasswordReset.objects.get(reset_key=reset_key)
            except:
                response_data = response_handler(
                    'FALSE_LINK',
                    'No such link found',
                    'This is a false link'
                )
                return Response(response_data, status=200)
            if qs.is_valid != True:
                response_data = response_handler(
                    'LINK_INVALID',
                    'link is not valid',
                    'The link is already used'
                )
                return Response(response_data, status=200)

            elif qs.key_expires < timezone.now():
                response_data = response_handler(
                    'LINK_EXPIRED',
                    'The link is expired',
                    'Request a new link'
                )
                return Response(response_data, status=200)
            else:
                response_data = response_handler(
                    'RESET_PASSWORD',
                    'Reset the password',
                    'request a new password'
                )
                return Response(response_data, status=200)


'''
{
"reset_key":"ef3d340ca6846adaee2dd52e57c0e1c77c66c8bd"
}
'''


class PasswordResetConfirmation(APIView):
    def post(self, request):
        serialized_data = PasswordResetConfirmationSerializer(data=request.data)
        if serialized_data.is_valid():
            temp_data = serialized_data.data
            reset_key = temp_data['reset_key']
            password = temp_data['password']
            confirm_password = temp_data['confirm_password']
            print(password, confirm_password)
            print(password != confirm_password)
            if password != confirm_password:
                response_data = response_handler(
                    'DIFFERENT_PASSWORD',
                    'enter same password',
                    'password should be same as confirm password'
                )
                print("not equal")
                return Response(response_data, status=200)
            else:
                print("equal")
                qs = PasswordReset.objects.get(reset_key=reset_key)
                # qs.is_valid = False
                qs.save(force_update=True)
                user_obj = qs.email
                user_obj.set_password(confirm_password)
                response_data = response_handler(
                    'PASSWORD_RESET_SUCCESS',
                    'password is reset',
                    'password reset is successfully verified'
                )
                return Response(response_data, status=200)


'''

{
"reset_key":"ef3d340ca6846adaee2dd52e57c0e1c77c66c8bd",
"password":"123456",
"confirm_password":"123456"
}     

'''
