from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from common.response_handler import response_handler


class LoginHandler(APIView):
    permission_classes = [IsAuthenticated]


    def get(self, request):

        if self.request.user.is_verified == False:
            response_data =  response_handler(
                'EMAIL_NOT_VERIFIED',
                'Email verification not done',
                'Your Email ID has not been verified',
            )
            return Response(data=response_data, status=400)
        elif self.request.user.has_filled_data == False :
            response_data = response_handler(
                'NOT_FILLED_BIO_DATA',
                'Bio Data not updated',
                'Your Bio data needs to be updated',
            )
            return Response(data=response_data, status=200)
        elif self.request.user.is_approved == False:
            response_data =  response_handler(
                'ACCOUNT_NOT_APPROVED',
                'Account not verified',
                'Your GCT Portal account should be verified by authorized person inorder to get approved',
            )
            return Response(data=response_data, status=400)
        else:
            response_data = response_handler(
                'CAN_LOGIN',
                'Login Granted',
                'Your can now login to your account',
            )
            return Response(data=response_data, status=200)