import hashlib

from django.utils import timezone
from django.utils.crypto import random
from django.views.generic import UpdateView
from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
    UpdateAPIView)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)
from rest_framework.response import Response
from rest_framework.views import APIView

from common.utility.email_utility import send_verification_mail
from ..serializers import *

'''
api/accounts/CustomUser/
'''


class CustomUserList(ListAPIView):
    serializer_class = CustomUserSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = ['=id','=email']

    def get_queryset(self, *args, **kwargs):
        queryset_list = CustomUser.objects.all()
        search_type = 'icontains'
        c = []
        for f in CustomUser._meta.get_fields():
            a = str(f)
            if 'accounts.CustomUser' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


class CustomUserCreate(CreateAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserCreateSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):

        if serializer.is_valid():
            random_number_string = str(random.random())
            random_number_string = random_number_string.encode('utf-8')
            salt = hashlib.sha1(random_number_string).hexdigest()[:5]
            salt = salt.encode('utf-8')
            usernamesalt = serializer.validated_data.get('email')
            usernamesalt = usernamesalt.encode('utf8')

            key = hashlib.sha1(salt + usernamesalt)
            key = key.hexdigest()
            activation_key = key
            key_expires = timezone.now() + timezone.timedelta(days=2)

            user = CustomUser.objects.create_user(
                email=serializer.validated_data.get('email'),
                password=serializer.validated_data.get('password'),
                is_staff_account=serializer.validated_data.get('is_staff_account'),
                activation_key=activation_key,
                key_expires=key_expires
            )

            send_verification_mail(user)
            return Response(CustomUserSerializer(user,context={'request': self.request}).data)


class CustomUserDetailedList(RetrieveAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    #
    # def get_serializer_context(self):
    #     return {'context': self.request.user}

class CustomUserUpdateList(RetrieveUpdateAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()

class AvatarUpdateView(RetrieveUpdateAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = AvatarUpdateSerializer
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()

class GetAvatarView(RetrieveAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = AvatarViewSerializer
    permission_classes = [IsAuthenticated]


class CustomUserDeleteList(DestroyAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]


class SelfDetail(APIView):
    def get(self, request):
        qs = request.user
        print(qs)
        print(request.user)
        details = []
        serial_data = CustomUserSerializerAll(qs, context={'request': request}).data
        details.append(serial_data)
        return Response(details)
