from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from ..serializers import *

'''
api/accounts/CustomUser/
'''


class programmeList(ListAPIView):
    serializer_class = programmeSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = programme.objects.all()
        search_type = 'icontains'
        c = []
        for f in programme._meta.get_fields():
            a = str(f)
            if 'accounts.programme' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


class programmeCreate(CreateAPIView):
    queryset = programme.objects.all()
    serializer_class = programmeSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class programmeDetailedList(RetrieveAPIView):
    queryset = programme.objects.all()
    serializer_class = programmeSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class programmeUpdateList(RetrieveUpdateAPIView):
    queryset = programme.objects.all()
    serializer_class = programmeSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class programmeDeleteList(DestroyAPIView):
    queryset = programme.objects.all()
    serializer_class = programmeSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
