from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)
from rest_framework.response import Response
from rest_framework.views import APIView

from ..serializers import *

'''
api/accounts/DepartmentSection/
'''


class DepartmentSectionList(ListAPIView):
    serializer_class = DepartmentSectionSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = ['=id', '=section_name']

    def get_queryset(self, *args, **kwargs):
        queryset_list = department_section.objects.all()

        search_type = 'icontains'
        c = []
        for f in department_section._meta.get_fields():
            a = str(f)
            if 'accounts.department_section' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


class DepartmentSectionCreate(CreateAPIView):
    queryset = department_section.objects.all()
    serializer_class = DepartmentSectionSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class DepartmentSectionDetailedList(RetrieveAPIView):
    queryset = department_section.objects.all()
    serializer_class = DepartmentSectionSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class DepartmentSectionDetailedLink(RetrieveAPIView):
    queryset = department_section.objects.all()
    serializer_class = DepartmentSectionLinkSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class DepartmentSectionUpdateList(RetrieveUpdateAPIView):
    queryset = department_section.objects.all()
    serializer_class = DepartmentSectionSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class DepartmentSectionDeleteList(DestroyAPIView):
    queryset = department_section.objects.all()
    serializer_class = DepartmentSectionSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]


class DS_GetAll(APIView):
    def get(self,request):
        list=[]
        qs = department_section.objects.all()
        for i in qs:
            a = i.__str__()
            b = i.id
            print(a, b)
            list.append({'id': b, 'ds': a })
        return Response(list)

'''
from itertools import chain

a=list(set(chain.from_iterable(
    (field.name, field.attname) if hasattr(field, 'attname') else (field.name,)
    for field in department_section._meta.get_fields()
    # For complete backwards compatibility, you may want to exclude
    # GenericForeignKey from the results.
    if not (field.many_to_one and field.related_model and field.foreign_key is None)
)))


a =[f.name for f in department_section._meta.get_fields()]
print(a)

a =[f.name for f in accounts.department_section._meta.get_fields()]
print(a)


c=[]
for f in department_section._meta.get_fields():
    a=str(f)
    if 'accounts.department_section' in a:
        c.append(str(f.name))
print(c)

    # print(f)
'''