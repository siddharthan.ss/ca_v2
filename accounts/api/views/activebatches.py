from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from ..serializers import *

'''
api/accounts/CustomUser/
'''


class ActiveBatchesList(ListAPIView):
    serializer_class = ActiveBatchesSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = active_batches.objects.all()
        search_type = 'icontains'
        c = []
        for f in active_batches._meta.get_fields():
            a = str(f)
            if 'accounts.active_batches' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list

class ActiveBatchesCreate(CreateAPIView):
    queryset = active_batches.objects.all()
    serializer_class = ActiveBatchesSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class ActiveBatchesDetailed(RetrieveAPIView):
    queryset = active_batches.objects.all()
    serializer_class = ActiveBatchesSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class ActiveBatchesDetailedLink(RetrieveAPIView):
    queryset = active_batches.objects.all()
    serializer_class = ActiveBatchesLinkSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class ActiveBatchesUpdate(RetrieveUpdateAPIView):
    queryset = active_batches.objects.all()
    serializer_class = ActiveBatchesSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class ActiveBatchesDelete(DestroyAPIView):
    queryset = active_batches.objects.all()
    serializer_class = ActiveBatchesSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
