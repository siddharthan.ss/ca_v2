from rest_framework import serializers
from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from versatileimagefield.serializers import VersatileImageFieldSerializer

from accounts.models import staff, student, CustomUser


class StaffProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = staff
        fields = '__all__'


class StudentProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = student
        fields = '__all__'


class CustomUserProfileSerializer(serializers.ModelSerializer):
    student_profile = serializers.SerializerMethodField()
    staff_profile = serializers.SerializerMethodField()
    avatar = VersatileImageFieldSerializer(
        sizes=[
            ('full_size', 'url'),
            ('thumbnail', 'thumbnail__100x100'),
            ('medium_square_crop', 'crop__400x400'),
            ('small_square_crop', 'crop__50x50')
        ]
    )

    def get_student_profile(self, obj):
        if student.objects.filter(user=obj).exists():
            return StudentProfileSerializer(student.objects.get(user=obj)).data
        else:
            return None

    def get_staff_profile(self, obj):
        print(obj)
        if staff.objects.filter(user=obj).exists():
            return StaffProfileSerializer(staff.objects.get(user=obj)).data
        else:
            return None

    class Meta:
        model = CustomUser
        # fields = '__all__'
        exclude = [
            'password',
            'activation_key',
            'key_expires',
        ]


class GetMyProfile(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        return Response(CustomUserProfileSerializer(request.user).data, 200)


class GetUserProfile(RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = CustomUserProfileSerializer
    queryset = CustomUser.objects.all()