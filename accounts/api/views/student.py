from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from ..serializers import *

'''
api/accounts/CustomUser/
'''


class StudentList(ListAPIView):
    serializer_class = StudentSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = student.objects.all()
        search_type = 'icontains'
        c = []
        for f in student._meta.get_fields():
            a = str(f)
            if 'accounts.student' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


class StudentCreate(CreateAPIView):
    queryset = student.objects.all()
    serializer_class = StudentSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class StudentDetailed(RetrieveAPIView):
    queryset = student.objects.all()
    serializer_class = StudentSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class StudentDetailedLink(RetrieveAPIView):
    queryset = student.objects.all()
    serializer_class = StudentLinkSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class StudentUpdate(RetrieveUpdateAPIView):
    queryset = student.objects.all()
    serializer_class = StudentSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class StudentDelete(DestroyAPIView):
    queryset = student.objects.all()
    serializer_class = StudentSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
