import re

from django.db.models.functions import datetime
from rest_framework import serializers
from rest_framework.relations import HyperlinkedIdentityField
from versatileimagefield.serializers import VersatileImageFieldSerializer

from accounts.models import *

custom_user_detail_url = HyperlinkedIdentityField(
    view_name='api_accounts:UserDetail',
    lookup_field='pk',
)

programme_detail_url = HyperlinkedIdentityField(
    view_name='api_accounts:programmeDetail',
    lookup_field='pk',
)
department_detail_url = HyperlinkedIdentityField(
    view_name='api_accounts:departmentDetail',
    lookup_field='pk',
)
department_section_detail_url = HyperlinkedIdentityField(
    view_name='api_accounts:DepartmentSectionDetail',
    lookup_field='pk',
)

class CustomUserCreateSerializer(serializers.ModelSerializer):


    def validate_email(self, value):
        print('email hello')
        email = value
        if not re.match(r'^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$', email):
            raise serializers.ValidationError("Please enter a valid email id")
        if CustomUser.objects.filter(email=email):
            raise serializers.ValidationError("This email id has already been registered")
        return email

    def validate_password(self,value):
        password = value
        if not re.match(r'(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&*-+=_/;:~.]).{8,}', password):
            raise serializers.ValidationError(
                "Password should contain atleast 8 characters as well as a character,a digit and a special character")
        return password

    class Meta:
        model = CustomUser
        fields = [
            'is_staff_account',
            'email',
            'password',
        ]
        extra_kwargs = {'password': {'write_only': True}}

# do not edit this
class CustomUserSerializer(serializers.ModelSerializer):
    avatar = VersatileImageFieldSerializer(
        sizes=[
            ('full_size', 'url'),
            ('thumbnail', 'thumbnail__100x100'),
            ('medium_square_crop', 'crop__400x400'),
            ('small_square_crop', 'crop__50x50')
        ]
    )

    class Meta:
        model = CustomUser
        fields = [
            'pk',
            'has_filled_data',
            'is_approved',
            'is_verified',
            'email',
            'password',
            'avatar'
        ]
        extra_kwargs = {'password': {'write_only': True}}

        def create(self, validated_data):
            user = CustomUser(
                email=validated_data['email'],
                username=validated_data['username']
            )
            user.create_user(self, user.email, user.password)

class AvatarUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomUser
        fields = [
            'avatar',
        ]

class AvatarViewSerializer(serializers.ModelSerializer):
    """Serializes Person instances"""
    avatar = VersatileImageFieldSerializer(
        sizes=[
            ('full_size', 'url'),
            ('thumbnail', 'thumbnail__100x100'),
            ('medium_square_crop', 'crop__400x400'),
            ('small_square_crop', 'crop__50x50')
        ]
    )


    class Meta:
        model = CustomUser
        fields = [
            'avatar',
        ]

class programmeSerializer(serializers.ModelSerializer):
    url = programme_detail_url

    class Meta:
        model = programme
        fields = ['url',
                  'pk',
                  'acronym',
                  'name',
                  ]


class departmentSerializer(serializers.ModelSerializer):
    url = department_detail_url

    class Meta:
        model = department
        fields = '__all__'


class DepartmentSectionSerializer(serializers.ModelSerializer):
    url = department_section_detail_url

    class Meta:
        model = department_section
        fields = '__all__'



class DepartmentSectionAllSerializer(serializers.ModelSerializer):

    class Meta:
        model = department_section
        fields = ('__str__',
                  'id',)


class DepartmentSectionLinkSerializer(serializers.ModelSerializer):
    url = department_section_detail_url
    department = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:departmentDetail'
    )

    programme = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:programmeDetail'
    )

    class Meta:
        model = department_section
        fields = '__all__'


regulation_detail_url = HyperlinkedIdentityField(
    view_name='api_accounts:RegulationDetail',
    lookup_field='pk',
)


class RegulationSerializer(serializers.ModelSerializer):
    url = regulation_detail_url

    class Meta:
        model = regulation
        fields = '__all__'


batch_detail_url = HyperlinkedIdentityField(
    view_name='api_accounts:BatchDetail',
    lookup_field='pk',
)


class BatchSerializer(serializers.ModelSerializer):
    url = batch_detail_url

    class Meta:
        model = batch
        fields = '__all__'


class BatchLinkSerializer(serializers.ModelSerializer):
    url = batch_detail_url
    programme = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:programmeDetail'
    )
    regulation = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:RegulationDetail'
    )

    class Meta:
        model = batch
        fields = '__all__'


active_batches_detail_url = HyperlinkedIdentityField(
    view_name='api_accounts:ActiveBatchDetail',
    lookup_field='pk',
)


class ActiveBatchesSerializer(serializers.ModelSerializer):
    url = active_batches_detail_url

    class Meta:
        model = active_batches
        fields = ''


class ActiveBatchesLinkSerializer(serializers.ModelSerializer):
    url = active_batches_detail_url
    batch = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:BatchDetail'
    )
    department_section = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:DepartmentSectionDetail'
    )

    class Meta:
        model = active_batches
        fields = '__all__'


staff_detail_url = HyperlinkedIdentityField(
    view_name='api_accounts:StaffDetail',
    lookup_field='pk',
)


class StaffSerializer(serializers.ModelSerializer):
    url = staff_detail_url

    class Meta:
        model = staff
        fields = '__all__'

    def validate_first_name(self, value):
        print('hello staff')
        first_name = value
        first_name = first_name.title()
        if not re.match(r'^([a-zA-Z]+)$', first_name):
            raise serializers.ValidationError("Enter a valid name")
        return first_name

    def validate_middle_name(self, value):
        middle_name = value
        middle_name = middle_name.title()
        if not re.match(r'^([a-zA-Z]*)$', middle_name):
            raise serializers.ValidationError("Enter a valid name")
        return middle_name

    def validate_last_name(self, value):
        last_name = value
        last_name = last_name.title()
        if not re.match(r'^([a-zA-Z ]+)$', last_name):
            raise serializers.ValidationError("Enter a valid name")
        return last_name

    def validate_dob(self, value):
        dob = value
        if not datetime.datetime.strptime(str(dob), '%Y-%m-%d'):
            raise serializers.ValidationError("Enter the date in the given format (YYYY-MM-DD)")
        return dob

    def validate_phone_number(self, value):
        phone_number = value
        if not re.match(r'^((\d+){10})$', phone_number):
            raise serializers.ValidationError("Enter a valid mobile number")
        return phone_number


    
class ApproveDisapproveSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomUser
        fields = [
            'is_approved'
        ]

class ApprovalListSerializer(serializers.ModelSerializer):
    custom_user = CustomUserSerializer(source='user')

    class Meta:
        model = staff
        fields = [
            'pk',
            'user',
            'first_name',
            'middle_name',
            'last_name',
            'designation',
            'custom_user',
        ]


class StaffLinkSerializer(serializers.ModelSerializer):
    url = staff_detail_url
    user = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:UserDetail'
    )
    qualification = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:programmeDetail'
    )
    department = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:departmentDetail'
    )

    class Meta:
        model = staff
        fields = '__all__'


student_detail_url = HyperlinkedIdentityField(
    view_name='api_accounts:StudentDetail',
    lookup_field='pk',
)


class StudentSerializer(serializers.ModelSerializer):
    url = student_detail_url

    class Meta:
        model = student
        fields = '__all__'

    def validate_roll_no(self, value):
        roll_no = value
        if student.objects.filter(roll_no=roll_no):
            raise serializers.ValidationError("This roll number has already been registered")
        if not re.match(r'^([0-9L]+)$', roll_no):
            raise serializers.ValidationError("Enter a valid roll_number")
        return roll_no

    def validate_first_name(self, value):
        print('hello student')
        first_name = value
        first_name = first_name.title()
        if not re.match(r'^([a-zA-Z]+)$', first_name):
            raise serializers.ValidationError("Enter a valid name")
        return first_name

    def validate_middle_name(self, value):
        middle_name = value
        middle_name = middle_name.title()
        if not re.match(r'^([a-zA-Z]*)$', middle_name):
            raise serializers.ValidationError("Enter a valid name")
        return middle_name

    def validate_last_name(self, value):
        last_name = value
        last_name = last_name.title()
        if not re.match(r'^([a-zA-Z ]+)$', last_name):
            raise serializers.ValidationError("Enter a valid name")
        return last_name

    def validate_dob(self, value):
        dob = value
        if not datetime.datetime.strptime(str(dob), '%Y-%m-%d'):
            raise serializers.ValidationError("Enter the date in the given format (YYYY-MM-DD)")
        return dob

    def validate_date_of_joining(self, value):
        dob = value
        if not datetime.datetime.strptime(str(dob), '%Y-%m-%d'):
            raise serializers.ValidationError("Enter the date in the given format (YYYY-MM-DD)")
        return dob

    def validate_phone_number(self, value):
        phone_number = value
        if not re.match(r'^((\d+){10})$', phone_number):
            raise serializers.ValidationError("Enter a valid mobile number of 10 digits")
        return phone_number


    # {
    #     "id": 2,
    #     "url": "http://localhost:8000/api/accounts/student/2/",
    #     "roll_no": "1517110",
    #     "first_name": "S",
    #     "middle_name": "",
    #     "last_name": "S",
    #     "gender": "M",
    #     "father_name": "s",
    #     "father_occupation": "s",
    #     "father_annual_income": 0.0,
    #     "mother_name": "s",
    #     "mother_occupation": "s",
    #     "mother_annual_income": 0.0,
    #     "temporary_address": "w",
    #     "permanent_address": "w",
    #     "dob": "2017-12-01",
    #     "date_of_joining": "2017-12-01",
    #     "hosteller_or_dayscholar": "hosteller",
    #     "entry_type": "regular",
    #     "phone_number": "9488608282",
    #     "caste": "ww",
    #     "community": "OC",
    #     "aadhaar_number": "undefined",
    #     "user": 2,
    #     "department_section": 1,
    #     "batch": 1,
    #     "programme": 1
    # }




class StudentLinkSerializer(serializers.ModelSerializer):
    url = student_detail_url
    user = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:UserDetail',
        lookup_field='pk',
    )
    department_section = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:DepartmentSectionDetail'
    )

    batch = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:BatchDetail'
    )
    programme = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:programmeDetail'
    )

    class Meta:
        model = student
        fields = '__all__'


class VerificationLinkSerializer(serializers.Serializer):
    verification_key = serializers.CharField()


class CustomUserSerializerAll(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = '__all__'


class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField()


class PasswordResetVerifySerializer(serializers.Serializer):
    reset_key = serializers.CharField(max_length=255)


class PasswordResetConfirmationSerializer(serializers.Serializer):
    reset_key = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=255)
    confirm_password = serializers.CharField(max_length=255)

class DS(serializers.Serializer):
    name = serializers.CharField(max_length=255)
    id = serializers.IntegerField()

