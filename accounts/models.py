import uuid

from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from rest_framework.reverse import reverse

# Create your models here.
from simple_history.models import HistoricalRecords
from versatileimagefield.fields import VersatileImageField


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password=None, is_staff_account=None, activation_key=None, key_expires=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        user = self.model(
            email=self.normalize_email(email),
            is_staff_account=is_staff_account,
            activation_key=activation_key,
            key_expires=key_expires
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, activation_key=None, key_expires=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            password=password,
            activation_key=activation_key,
            key_expires=key_expires,
            is_staff_account=True
        )
        user.is_approved = True
        user.is_superuser = True
        user.has_filled_data = True
        user.save(using=self._db)
        return user



def scramble_uploaded_filename(instance, filename):
    extension = filename.split(".")[-1]
    return "avatar/{}.{}".format(uuid.uuid4(), extension)

class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    is_staff_account = models.BooleanField(default=False)
    has_filled_data = models.BooleanField(default=False)
    is_approved = models.BooleanField(default=False)
    is_verified = models.BooleanField(default=False)
    activation_key = models.CharField(max_length=40)
    key_expires = models.DateTimeField()
    avatar = VersatileImageField('Avatar',upload_to=scramble_uploaded_filename,blank=True)
    objects = CustomUserManager()
    history = HistoricalRecords()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def __str__(self):  # __unicode__ on Python 2
        return self.email

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_superuser

    def get_absolute_url(self):
        return reverse("api_accounts:CustomUserDetailedList", kwargs={"pk": self.pk})


class programme(models.Model):
    name = models.CharField(max_length=30)
    acronym = models.CharField(max_length=5)

    def __str__(self):
        return str(self.acronym)

    def get_absolute_url(self):
        return reverse("api_accounts:programmeDetail", kwargs={"pk": self.pk})


class department(models.Model):
    name = models.CharField(max_length=50)
    acronym = models.CharField(max_length=5)
    department_code = models.SmallIntegerField()
    is_core = models.BooleanField()

    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        return reverse("api_accounts:departmentDetail", kwargs={"pk": self.pk})


class department_section(models.Model):
    department = models.ForeignKey(department)
    programme = models.ForeignKey(programme)
    section_name = models.CharField(max_length=1)

    def __str__(self):
        return str(self.department.acronym) + ' ' + str(self.programme.acronym) + ' ( ' + str(self.section_name) + ' )'

    def get_absolute_url(self):
        return reverse("api_accounts:DepartmentSectionDetail", kwargs={"pk": self.pk})


class regulation(models.Model):
    start_year = models.IntegerField()
    end_year = models.IntegerField()
    history = HistoricalRecords()

    def __str__(self):
        return str(self.start_year) + '-' + str(self.end_year)

    def get_absolute_url(self):
        return reverse("api_accounts:RegulationDetail", kwargs={"pk": self.pk})


class batch(models.Model):
    start_year = models.SmallIntegerField()
    end_year = models.SmallIntegerField()
    programme = models.ForeignKey(programme)
    regulation = models.ForeignKey(regulation)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.start_year) + '-' + str(self.end_year)+ '-'  + str(self.programme.acronym)

    def get_current_semester_number(self):
        # TODO
        pass

    def get_absolute_url(self):
        return reverse("api_accounts:BatchDetail", kwargs={"pk": self.pk})


class active_batches(models.Model):
    batch = models.ForeignKey(batch)
    department_section = models.ForeignKey(department_section)

    def get_absolute_url(self):
        return reverse("api_accounts:ActiveBatchDetail", kwargs={"pk": self.pk})

    def __str__(self):
        return str(self.batch) + ' ' + str(self.department_section)


GENDER_CHOICES = (
    ('M', 'Male'),
    ('F', 'Female'),
)


class student(models.Model):
    DAYSCHOLAR_HOSTELLER = (
        ('dayscholar', 'Day Scholar'),
        ('hosteller', 'Hosteller'),
    )

    ENTRY_TYPE = (
        ('regular', 'Regular'),
        ('lateral', 'Lateral'),
        ('transfer', 'Transfer'),
    )

    COMMUNITY = (
        ('OC', 'OC'),
        ('BC', 'BC'),
        ('BCM', 'BCM'),
        ('MBC', 'MBC'),
        ('SC', 'SC'),
        ('ST', 'ST'),
    )

    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    roll_no = models.CharField(unique=True, max_length=7)
    first_name = models.CharField(max_length=20)
    middle_name = models.CharField(max_length=20, blank=True)
    last_name = models.CharField(max_length=20)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, null=False)
    father_name = models.CharField(max_length=254)
    father_occupation = models.CharField(max_length=50, default=None)
    father_annual_income = models.FloatField(default=None,blank=True)
    mother_name = models.CharField(max_length=254)
    mother_occupation = models.CharField(max_length=50, default=None)
    mother_annual_income = models.FloatField(default=None,blank=True)
    department_section = models.ForeignKey(department_section)
    batch = models.ForeignKey(batch)
    programme = models.ForeignKey(programme)
    temporary_address = models.TextField(max_length=300)
    permanent_address = models.TextField(max_length=300)
    dob = models.DateField(verbose_name='Date of Birth')
    date_of_joining = models.DateField(null=True)
    hosteller_or_dayscholar = models.CharField(max_length=10, choices=DAYSCHOLAR_HOSTELLER, null=False)
    entry_type = models.CharField(max_length=10, choices=ENTRY_TYPE, null=False)
    phone_number = models.CharField(max_length=10)
    caste = models.CharField(max_length=50)
    community = models.CharField(max_length=3, choices=COMMUNITY)
    aadhaar_number = models.CharField(max_length=12, default=None, null=True, blank=True)

    history = HistoricalRecords()

    def __str__(self):
        name = str(self.first_name)
        if self.middle_name != None:
            name += ' '
            name += self.middle_name
        name += ' '
        name += self.last_name
        return name

    def get_absolute_url(self):
        return reverse("api_accounts:StudentDetail", kwargs={"pk": self.pk})

    class Meta:
        unique_together = (
            ('roll_no', 'aadhaar_number')
        )


class staff(models.Model):
    DESIGNATION_CHOICES = (
        ('Principal', 'Principal'),
        ('Professor', 'Professor'),
        ('Associate Professor', 'Associate Professor'),
        ('Assistant Professor', 'Assistant Professor'),
        ('Network Admin', 'Network Admin'),
        ('Alumni Staff', 'Alumni Staff'),
    )
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=20)
    middle_name = models.CharField(max_length=20, blank=True)
    last_name = models.CharField(max_length=20)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    dob = models.DateField(verbose_name='Date of Birth')
    department = models.ForeignKey(department, verbose_name='Department')
    designation = models.CharField(max_length=20, choices=DESIGNATION_CHOICES, )
    qualification = models.ForeignKey(programme)
    degree = models.CharField(max_length=254, blank=True)
    specialization = models.CharField(max_length=30)
    temporary_address = models.TextField(max_length=300)
    permanent_address = models.TextField(max_length=300)
    phone_number = models.CharField(max_length=10)

    history = HistoricalRecords()

    def __str__(self):
        name = str(self.first_name)
        if self.middle_name != None:
            name += ' '
            name += self.middle_name
        name += ' '
        name += self.last_name
        return name

    def get_absolute_url(self):
        return reverse("api_accounts:StaffDetail", kwargs={"pk": self.pk})


class PasswordReset(models.Model):
    email = models.ForeignKey(CustomUser)
    reset_key = models.CharField(max_length=255)
    key_expires = models.DateTimeField()
    is_valid = models.BooleanField(default=True)

    #
    # def create_PasswordReset(self,email,activation_key=None,key_expires=None,is_valid=None):
    #     PasswordReset = self.model(
    #         email=self.normalize_email(email),
    #         is_valid=is_valid,
    #         activation_key=activation_key,
    #         key_expires=key_expires,
    #     )
    #     PasswordReset.save(using=self._db)
    #     return PasswordReset
    def __str__(self):
        return str(self.is_valid) + ' ' + str(self.email) + ' ' + str(self.key_expires)

# def pre_save_PasswordReset_receiver(sender, instance, *args, **kwargs):
#     try:
#         qs=CustomUser.objects.get(email=instance.email)
#     except:
#         pass
#
# pre_save.connect(pre_save_PasswordReset_receiver, sender=PasswordReset)
