from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings


def send_verification_mail(customuser_instance):

    link = settings.ANGULAR_BASE_URL + '#/auth/verification?verification_key=' + customuser_instance.activation_key
    subject = 'GCT Portal - Account Verification'
    from_email = settings.EMAIL_HOST_USER

    msg_html = render_to_string('email_templates/verification_mail.html', {
        'verification_link': link
    })
    msg_plain = strip_tags(msg_html)
    send_mail(
        subject=subject,
        message=msg_plain,
        from_email=from_email,
        recipient_list = [customuser_instance.email],
        html_message=msg_html,
    )


def send_email(password_reset_instance):
    link = settings.ANGULAR_BASE_URL + '#/auth/password_reset?reset_key=' + str(
        password_reset_instance.reset_key)
    subject = 'GCT Portal - Password Reset'
    from_email = settings.EMAIL_HOST_USER
    print(password_reset_instance.reset_key)

    msg_html = render_to_string('email_templates/password_reset.html', {
        'reset_link': link
    })
    msg_plain = strip_tags(msg_html)
    send_mail(
        subject=subject,
        message=msg_plain,
        from_email=from_email,
        recipient_list=['hariprasathhari9292@gmail.com'],
        html_message=msg_html,
    )
