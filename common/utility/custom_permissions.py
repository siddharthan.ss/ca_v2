from rest_framework import permissions

class IsStaff(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.groups.filter(name='faculty').exists()

class IsPrincipal(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.groups.filter(name='principal').exists()

class IsHod(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.groups.filter(name='hod').exists()

class IsNetworkAdmin(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.groups.filter(name='network_admin').exists()

class IsProgrammeCoordinator(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.groups.filter(name='programme_coordinator').exists()

class IsFacultyAdvisor(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.groups.filter(name='faculty_advisor').exists()

class IsAlumniStaff(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.groups.filter(name='alumni_staff').exists()

class IsCoeStaff(permissions.BasePermission):
    
    def has_permission(self, request, view):
        return request.user.groups.filter(name='coe_staff').exists()