

def response_handler(response_code,response_message,response_explanation):
    response_dict = {}

    response_dict['response_code'] = response_code
    response_dict['response_message'] = response_message
    response_dict['response_explanation'] = response_explanation

    return response_dict
