from django.apps import AppConfig


class CourseEnrollmentConfig(AppConfig):
    name = 'course_enrollment'
