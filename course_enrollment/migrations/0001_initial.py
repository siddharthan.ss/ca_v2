# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-06-10 03:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('curriculum', '0001_initial'),
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='enrollment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('enrollment_date', models.DateField()),
                ('course', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='curriculum.course')),
                ('semester_duration', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='curriculum.semester_duration')),
                ('student', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.student')),
            ],
        ),
        migrations.CreateModel(
            name='lab_batch',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('student', models.ManyToManyField(to='accounts.student')),
                ('timetable', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='curriculum.timetable')),
            ],
        ),
        migrations.CreateModel(
            name='under_staff_allotment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('staff_allotment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='curriculum.staff_allotment')),
                ('student', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.student')),
            ],
        ),
    ]
