from django.conf.urls import url, include
from django.contrib import admin

from .views.enrollment import *
from .views.lab_batch import *
from .views.under_staff_allotment import *

admin.autodiscover()

urlpatterns = [
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),

    url(r'^Enrollment/create/$', EnrollmentCreate.as_view(), name='EnrollmentCreate'),
    url(r'^Enrollment/(?P<pk>[\w-]+)/edit/$', EnrollmentUpdate.as_view(), name='EnrollmentEdit'),
    url(r'^Enrollment/(?P<pk>[\w-]+)/delete/$', EnrollmentDelete.as_view(), name='EnrollmentDelete'),
    url(r'^Enrollment/(?P<pk>[\w-]+)/$', EnrollmentDetailed.as_view(), name='EnrollmentDetail'),
    url(r'^Enrollment/(?P<pk>[\w-]+)/link/$', EnrollmentDetailedLink.as_view(), name='EnrollmentDetailLink'),
    url(r'^Enrollment/$', EnrollmentList.as_view(), name='EnrollmentList'),

    url(r'^LabBatch/create/$', LabBatchCreate.as_view(), name='LabBatchCreate'),
    url(r'^LabBatch/(?P<pk>[\w-]+)/edit/$', LabBatchUpdate.as_view(), name='LabBatchEdit'),
    url(r'^LabBatch/(?P<pk>[\w-]+)/delete/$', LabBatchDelete.as_view(), name='LabBatchDelete'),
    url(r'^LabBatch/(?P<pk>[\w-]+)/$', LabBatchDetailed.as_view(), name='LabBatchDetail'),
    url(r'^LabBatch/(?P<pk>[\w-]+)/link/$', LabBatchDetailedLink.as_view(), name='LabBatchDetailLink'),
    url(r'^LabBatch/$', LabBatchList.as_view(), name='LabBatchList'),

    url(r'^UnderStaffAllotment/create/$', UnderStaffAllotmentCreate.as_view(), name='UnderStaffAllotmentCreate'),
    url(r'^UnderStaffAllotment/(?P<pk>[\w-]+)/edit/$', UnderStaffAllotmentUpdate.as_view(),
        name='UnderStaffAllotmentEdit'),
    url(r'^UnderStaffAllotment/(?P<pk>[\w-]+)/delete/$', UnderStaffAllotmentDelete.as_view(),
        name='UnderStaffAllotmentDelete'),
    url(r'^UnderStaffAllotment/(?P<pk>[\w-]+)/$', UnderStaffAllotmentDetailed.as_view(),
        name='UnderStaffAllotmentDetail'),
    url(r'^UnderStaffAllotment/(?P<pk>[\w-]+)/link/$', UnderStaffAllotmentDetailedLink.as_view(),
        name='UnderStaffAllotmentDetailLink'),
    url(r'^UnderStaffAllotment/$', UnderStaffAllotmentList.as_view(), name='UnderStaffAllotmentList'),

]
