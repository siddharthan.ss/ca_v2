from rest_framework import serializers
from rest_framework.relations import HyperlinkedIdentityField

from course_enrollment.models import *


class EnrollmentSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name='api_course_enrollment:EnrollmentDetail',
        lookup_field='pk',
    )

    class Meta:
        model = enrollment
        fields = '__all__'

class EnrollmentLinkSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name='api_course_enrollment:EnrollmentDetail',
        lookup_field='pk',
    )

    class Meta:
        model = enrollment
        fields = '__all__'

class UnderStaffAllotmentSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name='api_course_enrollment:UnderStaffAllotmentDetail',
        lookup_field='pk',
    )

    class Meta:
        model = under_staff_allotment
        fields = '__all__'

class UnderStaffAllotmentLinkSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name='api_course_enrollment:UnderStaffAllotmentDetail',
        lookup_field='pk',
    )

    class Meta:
        model = under_staff_allotment
        fields = '__all__'


class LabBatchSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name='api_course_enrollment:LabBatchDetail',
        lookup_field='pk',
    )

    class Meta:
        model = lab_batch
        fields = '__all__'

class LabBatchLinkSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name='api_course_enrollment:LabBatchDetail',
        lookup_field='pk',
    )

    class Meta:
        model = lab_batch
        fields = '__all__'

'''

class enrollment(models.Model):
    student = models.ForeignKey(student)
    semester_duration = models.ForeignKey(semester_duration)
    course = models.ForeignKey(course)
    enrollment_date = models.DateField()

class under_staff_allotment(models.Model):
    staff_allotment = models.ForeignKey(staff_allotment)
    student = models.ForeignKey(student)

class lab_batch(models.Model):
    timetable = models.ForeignKey(timetable)
    student = models.ManyToManyField(student)
'''

'''
student_detail_url = HyperlinkedIdentityField(
    view_name='api_accounts:StudentDetail',
    lookup_field='pk',
)


class StudentSerializer(serializers.ModelSerializer):
    url = student_detail_url

    class Meta:
        model = student
        fields = '__all__'


class StudentLinkSerializer(serializers.ModelSerializer):
    url = student_detail_url
    user = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:UserDetail',
        lookup_field='pk',
    )
    department_section = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:DepartmentSectionDetail'
    )

    batch = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:BatchDetail'
    )
    programme = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_accounts:programmeDetail'
    )

    class Meta:
        model = student
        fields = '__all__'
'''
