from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from ..serializers import *

'''
api/CourseUnderStaffAllotment/UnderStaffAllotment/
'''


class UnderStaffAllotmentList(ListAPIView):
    serializer_class = UnderStaffAllotmentSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = under_staff_allotment.objects.all()
        search_type = 'icontains'
        c = []
        for f in enrollment._meta.get_fields():
            a = str(f)
            if 'course_enrollment.under_staff_allotment' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


class UnderStaffAllotmentCreate(CreateAPIView):
    queryset = under_staff_allotment.objects.all()
    serializer_class = UnderStaffAllotmentSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class UnderStaffAllotmentDetailed(RetrieveAPIView):
    queryset = under_staff_allotment.objects.all()
    serializer_class = UnderStaffAllotmentSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class UnderStaffAllotmentDetailedLink(RetrieveAPIView):
    queryset = under_staff_allotment.objects.all()
    serializer_class = UnderStaffAllotmentLinkSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class UnderStaffAllotmentUpdate(RetrieveUpdateAPIView):
    queryset = under_staff_allotment.objects.all()
    serializer_class = UnderStaffAllotmentSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class UnderStaffAllotmentDelete(DestroyAPIView):
    queryset = under_staff_allotment.objects.all()
    serializer_class = UnderStaffAllotmentSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
