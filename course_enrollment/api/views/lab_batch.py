from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from ..serializers import *

'''
api/CourseLabBatch/LabBatch/
'''


class LabBatchList(ListAPIView):
    serializer_class = LabBatchSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = lab_batch.objects.all()
        search_type = 'icontains'
        c = []
        for f in enrollment._meta.get_fields():
            a = str(f)
            if 'course_enrollment.lab_batch' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


class LabBatchCreate(CreateAPIView):
    queryset = lab_batch.objects.all()
    serializer_class = LabBatchSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class LabBatchDetailed(RetrieveAPIView):
    queryset = lab_batch.objects.all()
    serializer_class = LabBatchSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class LabBatchDetailedLink(RetrieveAPIView):
    queryset = lab_batch.objects.all()
    serializer_class = LabBatchLinkSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class LabBatchUpdate(RetrieveUpdateAPIView):
    queryset = lab_batch.objects.all()
    serializer_class = LabBatchSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class LabBatchDelete(DestroyAPIView):
    queryset = lab_batch.objects.all()
    serializer_class = LabBatchSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
