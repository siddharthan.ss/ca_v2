from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from ..serializers import *

'''
api/CourseEnrollment/Enrollment/
'''


class EnrollmentList(ListAPIView):
    serializer_class = EnrollmentSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = enrollment.objects.all()
        search_type = 'icontains'
        c = []
        for f in enrollment._meta.get_fields():
            a = str(f)
            if 'course_enrollment.enrollment' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


'''
api/CourseEnrollment/Enrollment/Create
'''


class EnrollmentCreate(CreateAPIView):
    queryset = enrollment.objects.all()
    serializer_class = EnrollmentSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class EnrollmentDetailed(RetrieveAPIView):
    queryset = enrollment.objects.all()
    serializer_class = EnrollmentSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class EnrollmentDetailedLink(RetrieveAPIView):
    queryset = enrollment.objects.all()
    serializer_class = EnrollmentLinkSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class EnrollmentUpdate(RetrieveUpdateAPIView):
    queryset = enrollment.objects.all()
    serializer_class = EnrollmentSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class EnrollmentDelete(DestroyAPIView):
    queryset = enrollment.objects.all()
    serializer_class = EnrollmentSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
