from django.db import models
from rest_framework.reverse import reverse

from accounts.models import (
    student,
)
from curriculum.models import (
    semester_duration,
    course,
    staff_allotment,
    timetable
)


# Create your models here.

class enrollment(models.Model):
    student = models.ForeignKey(student)
    semester_duration = models.ForeignKey(semester_duration)
    course = models.ForeignKey(course)
    enrollment_date = models.DateField()

    def get_absolute_url(self):
        return reverse("api_course_enrollment:EnrollmentDetail", kwargs={"pk": self.pk})


class under_staff_allotment(models.Model):
    staff_allotment = models.ForeignKey(staff_allotment)
    student = models.ForeignKey(student)

    def get_absolute_url(self):
        return reverse("api_course_enrollment:UnderStaffAllotmentDetail", kwargs={"pk": self.pk})


class lab_batch(models.Model):
    timetable = models.ForeignKey(timetable)
    student = models.ManyToManyField(student)

    def get_absolute_url(self):
        return reverse("api_course_enrollment:LabBatchDetail", kwargs={"pk": self.pk})
