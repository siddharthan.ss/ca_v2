from django.db import models
from rest_framework.reverse import reverse

from accounts.models import student
from curriculum.models import staff_allotment


# Create your models here.

class attendance(models.Model):
    start_time = models.TimeField()
    end_time = models.TimeField()
    date = models.DateField()
    staff_allotment = models.ForeignKey(staff_allotment)
    present_students = models.ManyToManyField(student,related_name='present_students')
    absent_students = models.ManyToManyField(student,related_name='absent_students')
    onduty_students = models.ManyToManyField(student,related_name='onduty_students')

    def get_absolute_url(self):
        return reverse(viewname='api_attendance:AttendanceDetail', kwargs={"pk": self.pk})
