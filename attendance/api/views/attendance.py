from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from ..serializer import *

'''
api/accounts/CustomUser/
'''


class AttendanceList(ListAPIView):
    serializer_class = AttendanceSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = attendance.objects.all()
        search_type = 'icontains'
        c = []
        for f in attendance._meta.get_fields():
            a = str(f)
            if 'attendance.attendance' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


class AttendanceCreate(CreateAPIView):
    queryset = attendance.objects.all()
    serializer_class = AttendanceSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class AttendanceDetailed(RetrieveAPIView):
    queryset = attendance.objects.all()
    serializer_class = AttendanceSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class AttendanceDetailedLink(RetrieveAPIView):
    queryset = attendance.objects.all()
    serializer_class = AttendanceLinkSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class AttendanceUpdate(RetrieveUpdateAPIView):
    queryset = attendance.objects.all()
    serializer_class = AttendanceSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class AttendanceDelete(DestroyAPIView):
    queryset = attendance.objects.all()
    serializer_class = AttendanceSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
