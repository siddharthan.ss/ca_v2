from rest_framework import serializers
from rest_framework.relations import HyperlinkedIdentityField

from attendance.models import *


class AttendanceSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name='api_attendance:AttendanceDetail',
        lookup_field='pk',
    )
    class Meta:
        model = attendance
        fields = '__all__'


class AttendanceLinkSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name='api_attendance:AttendanceDetail',
        lookup_field='pk',
    )
    class Meta:
        model = attendance
        fields = '__all__'
