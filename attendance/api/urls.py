from django.conf.urls import url, include
from django.contrib import admin

from .views.attendance import *

admin.autodiscover()

urlpatterns = [
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),

    url(r'^Attendance/create/$', AttendanceCreate.as_view(), name='AttendanceCreate'),
    url(r'^Attendance/(?P<pk>[\w-]+)/edit/$', AttendanceUpdate.as_view(), name='AttendanceEdit'),
    url(r'^Attendance/(?P<pk>[\w-]+)/delete/$', AttendanceDelete.as_view(), name='AttendanceDelete'),
    url(r'^Attendance/(?P<pk>[\w-]+)/$', AttendanceDetailed.as_view(), name='AttendanceDetail'),
    url(r'^Attendance/$', AttendanceList.as_view(), name='AttendanceList'),

]
