# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-06-10 03:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='active_semester_duration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('batch', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.batch')),
            ],
        ),
        migrations.CreateModel(
            name='course',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('semester', models.SmallIntegerField(blank=True, null=True)),
                ('subject_code', models.CharField(max_length=8)),
                ('course_name', models.CharField(max_length=100)),
                ('course_short_code', models.CharField(blank=True, max_length=10)),
                ('subject_type', models.CharField(choices=[('T', 'Theory'), ('P', 'Practical')], max_length=9)),
                ('CAT', models.CharField(choices=[('HS', 'HS'), ('BS', 'BS'), ('ES', 'ES')], max_length=2)),
                ('CAT_marks', models.SmallIntegerField()),
                ('end_semester_marks', models.SmallIntegerField()),
                ('L_credits', models.SmallIntegerField()),
                ('T_credits', models.SmallIntegerField()),
                ('P_credits', models.SmallIntegerField()),
                ('C_credits', models.SmallIntegerField()),
                ('is_elective', models.BooleanField(default=False)),
                ('is_open', models.BooleanField(default=False)),
                ('is_one_credit', models.BooleanField(default=False)),
                ('common_course', models.BooleanField(default=False)),
                ('department', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.department', verbose_name='Department')),
                ('programme', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.programme')),
                ('regulation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.regulation')),
            ],
        ),
        migrations.CreateModel(
            name='fixed_hour_duration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_time', models.TimeField()),
                ('end_time', models.TimeField()),
                ('hour', models.SmallIntegerField()),
                ('year', models.SmallIntegerField()),
                ('regulation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.regulation')),
            ],
        ),
        migrations.CreateModel(
            name='Historicalcourse',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('semester', models.SmallIntegerField(blank=True, null=True)),
                ('subject_code', models.CharField(max_length=8)),
                ('course_name', models.CharField(max_length=100)),
                ('course_short_code', models.CharField(blank=True, max_length=10)),
                ('subject_type', models.CharField(choices=[('T', 'Theory'), ('P', 'Practical')], max_length=9)),
                ('CAT', models.CharField(choices=[('HS', 'HS'), ('BS', 'BS'), ('ES', 'ES')], max_length=2)),
                ('CAT_marks', models.SmallIntegerField()),
                ('end_semester_marks', models.SmallIntegerField()),
                ('L_credits', models.SmallIntegerField()),
                ('T_credits', models.SmallIntegerField()),
                ('P_credits', models.SmallIntegerField()),
                ('C_credits', models.SmallIntegerField()),
                ('is_elective', models.BooleanField(default=False)),
                ('is_open', models.BooleanField(default=False)),
                ('is_one_credit', models.BooleanField(default=False)),
                ('common_course', models.BooleanField(default=False)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('department', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='accounts.department')),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='accounts.CustomUser')),
                ('programme', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='accounts.programme')),
                ('regulation', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='accounts.regulation')),
            ],
            options={
                'get_latest_by': 'history_date',
                'ordering': ('-history_date', '-history_id'),
                'verbose_name': 'historical course',
            },
        ),
        migrations.CreateModel(
            name='offered_course',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('course', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='curriculum.course')),
            ],
        ),
        migrations.CreateModel(
            name='semester_duration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('year', models.IntegerField()),
                ('is_odd_semester', models.BooleanField()),
                ('department_section', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.department_section')),
            ],
        ),
        migrations.CreateModel(
            name='staff_allotment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('offered_course', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='curriculum.offered_course')),
                ('semester_duration', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='curriculum.semester_duration')),
                ('staff', models.ManyToManyField(to='accounts.staff')),
            ],
        ),
        migrations.CreateModel(
            name='timetable',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_time', models.TimeField()),
                ('end_time', models.TimeField()),
                ('date', models.DateField()),
                ('repeat_mode', models.CharField(choices=[('O', 'Once'), ('W', 'Weekly')], max_length=1)),
                ('effect_from_date', models.DateField()),
                ('valid_upto_date', models.DateField()),
                ('staff_allotment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='curriculum.staff_allotment')),
            ],
        ),
        migrations.AddField(
            model_name='offered_course',
            name='semester_duration',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='curriculum.semester_duration'),
        ),
        migrations.AddField(
            model_name='active_semester_duration',
            name='semester_duration',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='curriculum.semester_duration'),
        ),
        migrations.AlterUniqueTogether(
            name='course',
            unique_together=set([('subject_code', 'course_short_code')]),
        ),
    ]
