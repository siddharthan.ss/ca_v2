from django.db import models
from rest_framework.reverse import reverse
from simple_history.models import HistoricalRecords

from accounts.models import (
    department_section,
    regulation,
    department,
    programme,
    staff,
    batch,
)


# Create your models here.

class semester_duration(models.Model):
    year = models.IntegerField()
    is_odd_semester = models.BooleanField()
    department_section = models.ForeignKey(department_section)

    def get_absolute_url(self):
        return reverse("api_curriculum:SemesterDurationDetail", kwargs={"pk": self.pk})

class programme_coordinator(models.Model):
    staff = models.ForeignKey(staff)
    department = models.ForeignKey(department)
    programme = models.ForeignKey(programme)


class active_semester_duration(models.Model):
    semester_duration = models.ForeignKey(semester_duration)
    batch = models.ForeignKey(batch)

    def get_absolute_url(self):
        return reverse("api_curriculum:ActiveSemesterDurationDetail", kwargs={"pk": self.pk})


class fixed_hour_duration(models.Model):
    start_time = models.TimeField()
    end_time  = models.TimeField()
    hour = models.SmallIntegerField()
    regulation = models.ForeignKey(regulation)
    year = models.SmallIntegerField()

    def get_absolute_url(self):
        return reverse("api_curriculum:FixedHourDurationDetail", kwargs={"pk": self.pk})


class course(models.Model):
    CAT_CHOICES = (
        ('HS', 'HS'),
        ('BS', 'BS'),
        ('ES', 'ES'),
    )
    SUBJECT_CHOICES = (
        ('T', 'Theory'),
        ('P', 'Practical'),
    )
    department = models.ForeignKey(department, verbose_name='Department')
    semester = models.SmallIntegerField(blank=True, null=True)
    subject_code = models.CharField(max_length=8)
    course_name = models.CharField(max_length=100)
    course_short_code = models.CharField(blank=True, max_length=10)
    subject_type = models.CharField(max_length=9, choices=SUBJECT_CHOICES)
    CAT = models.CharField(max_length=2, choices=CAT_CHOICES)
    CAT_marks = models.SmallIntegerField()
    end_semester_marks = models.SmallIntegerField()
    L_credits = models.SmallIntegerField()
    T_credits = models.SmallIntegerField()
    P_credits = models.SmallIntegerField()
    C_credits = models.SmallIntegerField()
    programme = models.ForeignKey(programme)
    is_elective = models.BooleanField(default=False)
    is_open = models.BooleanField(default=False)
    is_one_credit = models.BooleanField(default=False)
    regulation = models.ForeignKey(regulation)
    common_course = models.BooleanField(default=False)
    history = HistoricalRecords()

    class Meta:
        unique_together = ('subject_code', 'course_short_code')

    def __str__(self):
        if self.common_course:
            return str(self.course_name)
        return str(self.course_id) + '-' + str(self.course_name)

    def get_absolute_url(self):
        return reverse("api_curriculum:CourseDetail", kwargs={"pk": self.pk})


class offered_course(models.Model):
    course = models.ForeignKey(course)
    semester_duration = models.ForeignKey(semester_duration)

    def get_absolute_url(self):
        return reverse("api_curriculum:OfferedCourseDetail", kwargs={"pk": self.pk})


class staff_allotment(models.Model):
    semester_duration = models.ForeignKey(semester_duration)
    offered_course = models.ForeignKey(offered_course)
    staff = models.ManyToManyField(staff)

    def get_absolute_url(self):
        return reverse("api_curriculum:StaffAllotmentDetail", kwargs={"pk": self.pk})


class timetable(models.Model):
    REPEAT_MODES = (
        ('O','Once'),
        ('W','Weekly')
    )
    start_time = models.TimeField()
    end_time = models.TimeField()
    staff_allotment = models.ForeignKey(staff_allotment)
    date = models.DateField()
    repeat_mode = models.CharField(max_length=1, choices=REPEAT_MODES)
    effect_from_date = models.DateField()
    valid_upto_date = models.DateField()

    def get_absolute_url(self):
        return reverse("api_curriculum:TimetableDetail", kwargs={"pk": self.pk})
