from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from ..serializers import *

'''
api/Curriculum/FixedHourDuration/
'''


class FixedHourDurationList(ListAPIView):
    serializer_class = FixedHourDurationSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = fixed_hour_duration.objects.all()
        search_type = 'icontains'
        c = []
        for f in fixed_hour_duration._meta.get_fields():
            a = str(f)
            if 'curriculum.fixed_hour_duration' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


class FixedHourDurationCreate(CreateAPIView):
    queryset = fixed_hour_duration.objects.all()
    serializer_class = FixedHourDurationSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class FixedHourDurationDetailed(RetrieveAPIView):
    queryset = fixed_hour_duration.objects.all()
    serializer_class = FixedHourDurationSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class FixedHourDurationDetailedLink(RetrieveAPIView):
    queryset = fixed_hour_duration.objects.all()
    # serializer_class = FixedHourDurationLinkSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class FixedHourDurationUpdate(RetrieveUpdateAPIView):
    queryset = fixed_hour_duration.objects.all()
    serializer_class = FixedHourDurationSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class FixedHourDurationDelete(DestroyAPIView):
    queryset = fixed_hour_duration.objects.all()
    serializer_class = FixedHourDurationSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
