from rest_framework import generics
from rest_framework import serializers
from rest_framework.generics import CreateAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from accounts.api.serializers import StaffSerializer
from accounts.models import staff, department, programme
from common.response_handler import response_handler
from common.utility.custom_permissions import IsHod
from curriculum.models import programme_coordinator


class PCSerializer(serializers.ModelSerializer):

    class Meta:
        model = programme_coordinator
        fields = [
            'staff',
            'programme',
        ]

class GetStaffsOfGivenDepartment(generics.ListAPIView):

    permission_classes = [IsAuthenticated]

    serializer_class = StaffSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = None
        got_department = self.request.query_params.get('department_pk', None)
        if got_department is not None:
            queryset = staff.objects.filter(department=got_department).filter(user__is_approved=True)
        return queryset

class AssignPC(APIView):

    permission_classes = [IsAuthenticated, IsHod]

    def get(self,request):
        return Response(PCSerializer(programme_coordinator.objects.all(),many=True).data)

    def post(self,request):
        serialized_data = PCSerializer(data=request.data)
        if  serialized_data.is_valid():
            validated_data = serialized_data.data

            staff_instance = staff.objects.get(pk=validated_data.get('staff'))
            requested_staff_instance = staff.objects.get(user=request.user)
            department_instance = requested_staff_instance.department
            programme_instance = programme.objects.get(pk=validated_data.get('programme'))

            if programme_coordinator.objects.filter(
                department=department_instance,
                programme= programme_instance
            ).exists():
                pc_instance = programme_coordinator.objects.get(
                    department = department_instance,
                    programme = programme_instance,
                )
                pc_instance.staff = staff_instance
                pc_instance.save()
            else:
                pc_instance = programme_coordinator.objects.create(
                    staff=staff_instance,
                    department=department_instance,
                    programme=programme_instance,
                )

            return Response(pc_instance,status=200)
        else:
            response_data = response_handler(
                'PC_ASSIGN_ERROR',
                'Error Assigning',
                'Unable to assign the selected programme co-ordinator'
            )
            return Response(data=response_data,status=400)