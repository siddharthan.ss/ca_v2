from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from ..serializers import *

'''
/api/Curriculum/ActiveSemesterDuration/
'''


class ActiveSemesterDurationList(ListAPIView):
    serializer_class = ActiveSemesterDurationSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = active_semester_duration.objects.all()
        search_type = 'icontains'
        c = []
        for f in active_semester_duration._meta.get_fields():
            a = str(f)
            if 'curriculum.active_semester_duration' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


'''
 /api/Curriculum/ActiveSemesterDuration/create/
'''


class ActiveSemesterDurationCreate(CreateAPIView):
    queryset = active_semester_duration.objects.all()
    serializer_class = ActiveSemesterDurationSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


'''
 /api/Curriculum/ActiveSemesterDuration/1/edit/
'''


class ActiveSemesterDurationDetailed(RetrieveAPIView):
    queryset = active_semester_duration.objects.all()
    serializer_class = ActiveSemesterDurationSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


'''
 /api/Curriculum/ActiveSemesterDuration/1/link/
'''


class ActiveSemesterDurationDetailedLink(RetrieveAPIView):
    queryset = active_semester_duration.objects.all()
    # serializer_class = ActiveSemesterDurationLinkSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


'''
 /api/Curriculum/ActiveSemesterDuration/1/edit/
'''


class ActiveSemesterDurationUpdate(RetrieveUpdateAPIView):
    queryset = active_semester_duration.objects.all()
    serializer_class = ActiveSemesterDurationSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


'''
 /api/Curriculum/ActiveSemesterDuration/1/delete/
'''


class ActiveSemesterDurationDelete(DestroyAPIView):
    queryset = active_semester_duration.objects.all()
    serializer_class = ActiveSemesterDurationSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
