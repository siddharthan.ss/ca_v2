from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from ..serializers import *

'''
api/accounts/OfferedCourse/
'''


class OfferedCourseList(ListAPIView):
    serializer_class = OfferedCourseSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = offered_course.objects.all()
        search_type = 'icontains'
        c = []
        for f in offered_course._meta.get_fields():
            a = str(f)
            if 'curriculum.offered_course' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


class OfferedCourseCreate(CreateAPIView):
    queryset = offered_course.objects.all()
    serializer_class = OfferedCourseSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class OfferedCourseDetailed(RetrieveAPIView):
    queryset = offered_course.objects.all()
    serializer_class = OfferedCourseSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class OfferedCourseDetailedLink(RetrieveAPIView):
    queryset = offered_course.objects.all()
    # serializer_class = OfferedCourseLinkSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class OfferedCourseUpdate(RetrieveUpdateAPIView):
    queryset = offered_course.objects.all()
    serializer_class = OfferedCourseSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class OfferedCourseDelete(DestroyAPIView):
    queryset = offered_course.objects.all()
    serializer_class = OfferedCourseSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
