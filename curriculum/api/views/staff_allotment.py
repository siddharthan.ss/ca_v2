from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from ..serializers import *

'''
api/accounts/StaffAllotment/
'''


class StaffAllotmentList(ListAPIView):
    serializer_class = StaffAllotmentSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = staff_allotment.objects.all()
        search_type = 'icontains'
        c = []
        for f in staff_allotment._meta.get_fields():
            a = str(f)
            if 'curriculum.staff_allotment' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


class StaffAllotmentCreate(CreateAPIView):
    queryset = staff_allotment.objects.all()
    serializer_class = StaffAllotmentSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class StaffAllotmentDetailed(RetrieveAPIView):
    queryset = staff_allotment.objects.all()
    serializer_class = StaffAllotmentSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class StaffAllotmentDetailedLink(RetrieveAPIView):
    queryset = staff_allotment.objects.all()
    # serializer_class = StaffAllotmentLinkSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class StaffAllotmentUpdate(RetrieveUpdateAPIView):
    queryset = staff_allotment.objects.all()
    serializer_class = StaffAllotmentSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class StaffAllotmentDelete(DestroyAPIView):
    queryset = staff_allotment.objects.all()
    serializer_class = StaffAllotmentSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
