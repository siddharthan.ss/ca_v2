from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from ..serializers import *

'''
api/accounts/SemesterDuration/
'''


class SemesterDurationList(ListAPIView):
    serializer_class = SemesterDurationSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = semester_duration.objects.all()
        search_type = 'icontains'
        c = []
        for f in semester_duration._meta.get_fields():
            a = str(f)
            if 'curriculum.semester_duration' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     # Q(acronym__icontains=query) |
                                                     # Q(name__icontains=query)
                                                     )
        return queryset_list


class SemesterDurationCreate(CreateAPIView):
    queryset = semester_duration.objects.all()
    serializer_class = SemesterDurationSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class SemesterDurationDetailed(RetrieveAPIView):
    queryset = semester_duration.objects.all()
    serializer_class = SemesterDurationSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class SemesterDurationDetailedLink(RetrieveAPIView):
    queryset = semester_duration.objects.all()
    # serializer_class = SemesterDurationLinkSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class SemesterDurationUpdate(RetrieveUpdateAPIView):
    queryset = semester_duration.objects.all()
    serializer_class = SemesterDurationSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class SemesterDurationDelete(DestroyAPIView):
    queryset = semester_duration.objects.all()
    serializer_class = SemesterDurationSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
