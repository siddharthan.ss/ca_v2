from django.conf.urls import url, include
from django.contrib import admin

from .views.active_semester_duration import *
from .views.course import *
from .views.fixed_hour_duration import *
from .views.offered_course import *
from .views.semester_duration import *
from .views.staff_allotment import *
from .views.timetable import *
from .views.assign_pc import *

admin.autodiscover()


urlpatterns = [
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),

    url(r'^ActiveSemesterDuration/create/$', ActiveSemesterDurationCreate.as_view(), name='ActiveSemesterDurationCreate'),
    url(r'^ActiveSemesterDuration/(?P<pk>[\w-]+)/edit/$', ActiveSemesterDurationUpdate.as_view(), name='ActiveSemesterDurationEdit'),
    url(r'^ActiveSemesterDuration/(?P<pk>[\w-]+)/delete/$', ActiveSemesterDurationDelete.as_view(), name='ActiveSemesterDurationDelete'),
    url(r'^ActiveSemesterDuration/(?P<pk>[\w-]+)/$', ActiveSemesterDurationDetailed.as_view(), name='ActiveSemesterDurationDetail'),
    url(r'^ActiveSemesterDuration/$', ActiveSemesterDurationList.as_view(), name='ActiveSemesterDurationList'),

    url(r'^Course/create/$', CourseCreate.as_view(), name='CourseCreate'),
    url(r'^Course/(?P<pk>[\w-]+)/edit/$', CourseUpdate.as_view(), name='CourseEdit'),
    url(r'^Course/(?P<pk>[\w-]+)/delete/$', CourseDelete.as_view(), name='CourseDelete'),
    url(r'^Course/(?P<pk>[\w-]+)/$', CourseDetailed.as_view(), name='CourseDetail'),
    url(r'^Course/$', CourseList.as_view(), name='CourseList'),

    url(r'^FixedHourDuration/create/$', FixedHourDurationCreate.as_view(), name='FixedHourDurationCreate'),
    url(r'^FixedHourDuration/(?P<pk>[\w-]+)/edit/$', FixedHourDurationUpdate.as_view(), name='FixedHourDurationEdit'),
    url(r'^FixedHourDuration/(?P<pk>[\w-]+)/delete/$', FixedHourDurationDelete.as_view(), name='FixedHourDurationDelete'),
    url(r'^FixedHourDuration/(?P<pk>[\w-]+)/$', FixedHourDurationDetailed.as_view(), name='FixedHourDurationDetail'),
    url(r'^FixedHourDuration/$', FixedHourDurationList.as_view(), name='FixedHourDurationList'),

    url(r'^OfferedCourse/create/$', OfferedCourseCreate.as_view(), name='OfferedCourseCreate'),
    url(r'^OfferedCourse/(?P<pk>[\w-]+)/edit/$', OfferedCourseUpdate.as_view(),
        name='OfferedCourseEdit'),
    url(r'^OfferedCourse/(?P<pk>[\w-]+)/delete/$', OfferedCourseDelete.as_view(),
        name='OfferedCourseDelete'),
    url(r'^OfferedCourse/(?P<pk>[\w-]+)/$', OfferedCourseDetailed.as_view(),
        name='OfferedCourseDetail'),
    url(r'^OfferedCourse/(?P<pk>[\w-]+)/link/$', OfferedCourseDetailedLink.as_view(),
        name='OfferedCourseDetailLink'),
    url(r'^OfferedCourse/$', OfferedCourseList.as_view(), name='OfferedCourseList'),

    url(r'^SemesterDuration/create/$', SemesterDurationCreate.as_view(), name='SemesterDurationCreate'),
    url(r'^SemesterDuration/(?P<pk>[\w-]+)/edit/$', SemesterDurationUpdate.as_view(), name='SemesterDurationEdit'),
    url(r'^SemesterDuration/(?P<pk>[\w-]+)/delete/$', SemesterDurationDelete.as_view(), name='SemesterDurationDelete'),
    url(r'^SemesterDuration/(?P<pk>[\w-]+)/$', SemesterDurationDetailed.as_view(), name='SemesterDurationDetail'),
    url(r'^SemesterDuration/(?P<pk>[\w-]+)/link/$', SemesterDurationDetailedLink.as_view(), name='SemesterDurationDetailLink'),
    url(r'^SemesterDuration/$', SemesterDurationList.as_view(), name='SemesterDurationList'),

    url(r'^Timetable/create/$', TimetableCreate.as_view(), name='TimetableCreate'),
    url(r'^Timetable/(?P<pk>[\w-]+)/edit/$', TimetableUpdate.as_view(), name='TimetableEdit'),
    url(r'^Timetable/(?P<pk>[\w-]+)/delete/$', TimetableDelete.as_view(), name='TimetableDelete'),
    url(r'^Timetable/(?P<pk>[\w-]+)/$', TimetableDetailed.as_view(), name='TimetableDetail'),
    url(r'^Timetable/(?P<pk>[\w-]+)/link/$', TimetableDetailedLink.as_view(), name='TimetableDetailLInk'),
    url(r'^Timetable/$', TimetableList.as_view(), name='TimetableList'),

    url(r'^StaffAllotment/create/$', StaffAllotmentCreate.as_view(), name='StaffAllotmentCreate'),
    url(r'^StaffAllotment/(?P<pk>[\w-]+)/edit/$', StaffAllotmentUpdate.as_view(), name='StaffAllotmentEdit'),
    url(r'^StaffAllotment/(?P<pk>[\w-]+)/delete/$', StaffAllotmentDelete.as_view(), name='StaffAllotmentDelete'),
    url(r'^StaffAllotment/(?P<pk>[\w-]+)/$', StaffAllotmentDetailed.as_view(), name='StaffAllotmentDetail'),
    url(r'^StaffAllotment/(?P<pk>[\w-]+)/link/$', StaffAllotmentDetailedLink.as_view(), name='StaffAllotmentDetailLink'),
    url(r'^StaffAllotment/$', StaffAllotmentList.as_view(), name='StaffAllotmentList'),


    # # assign PC
    # url(r'^get_current_pc/$', GetCurrentPC.as_view(), name='GetCurrentPC'),
    # url(r'^assign_pc/$', StaffAllotmentList.as_view(), name='StaffAllotmentList'),

    url(r'^assign_pc/$', AssignPC.as_view(), name='assign_pc'),
    url(r'^get_staffs_of_given_department/$', GetStaffsOfGivenDepartment.as_view(), name='get_staffs_of_given_department'),
]
