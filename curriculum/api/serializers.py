from rest_framework import serializers
from rest_framework.relations import HyperlinkedIdentityField

from curriculum.models import *


class ActiveSemesterDurationSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name=' api_curriculum:ActiveSemesterDurationDetail',
        lookup_field='pk',
    )

    class Meta:
        model = active_semester_duration
        fields = '__all__'


class ActiveSemesterDurationLinkSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name='api_curriculum:ActiveSemesterDurationDetail',
        lookup_field='pk',
    )
    semester_duration = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_curriculum:SemesterDurationDetail'
    )
    batch = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_curriculum:StaffAllotmentDetail'
    )

    class Meta:
        model = active_semester_duration
        fields = '__all__'


class CourseSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name=' api_curriculum:CourseDetail',
        lookup_field='pk',
    )

    class Meta:
        model = course
        fields = '__all__'


class CourseLinkSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name=' api_curriculum:CourseDetail',
        lookup_field='pk',
    )
    programme = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_curriculum:StaffAllotmentDetail'
    )
    regulation =  serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_curriculum:StaffAllotmentDetail'
    )

    class Meta:
        model = course
        fields = '__all__'


class FixedHourDurationSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name=' api_curriculum:FixedHourDurationDetail',
        lookup_field='pk',
    )

    class Meta:
        model = fixed_hour_duration
        fields = '__all__'


class FixedHourDurationLinkSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name=' api_curriculum:FixedHourDurationDetail',
        lookup_field='pk',
    )

    class Meta:
        model = fixed_hour_duration
        fields = '__all__'


class OfferedCourseSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name=' api_curriculum:OfferedCourseDetail',
        lookup_field='pk',
    )

    class Meta:
        model = offered_course
        fields = '__all__'


class OfferedCourseLinkSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name=' api_curriculum:OfferedCourseDetail',
        lookup_field='pk',
    )


    course = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_curriculum:CourseDetail'
    )
    semester_duration = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_curriculum:SemesterDurationDetail'
    )

    class Meta:
        model = offered_course
        fields = '__all__'


class SemesterDurationSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name=' api_curriculum:SemesterDurationDetail',
        lookup_field='pk',
    )

    class Meta:
        model = semester_duration
        fields = '__all__'


class SemesterDurationLinkSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name=' api_curriculum:SemesterDurationDetail',
        lookup_field='pk',
    )
    department_section =  serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_curriculum:DepartmentSectionDetail'
    )


    class Meta:
        model = semester_duration
        fields = '__all__'


class StaffAllotmentSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name=' api_curriculum:StaffAllotmentDetail',
        lookup_field='pk',
    )

    class Meta:
        model = staff_allotment
        fields = '__all__'


class StaffAllotmentLinkSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name=' api_curriculum:StaffAllotmentDetail',
        lookup_field='pk',
    )
    semester_duration = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_curriculum:SemesterDurationDetail'
    )
    offered_course =  serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_curriculum:OfferedCourseDetail'
    )
    staff = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_curriculum:StaffDetail'
    )

    class Meta:
        model = staff_allotment
        fields = '__all__'


class TimetableSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name=' api_curriculum:TimetableDetail',
        lookup_field='pk',
    )

    class Meta:
        model = timetable
        fields = '__all__'


class TimetableLinkSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name=' api_curriculum:TimetablehDetail',
        lookup_field='pk',
    )
    staff_allotment = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api_curriculum:StaffAllotmentDetail'
    )


    class Meta:
        model = timetable
        fields = '__all__'
