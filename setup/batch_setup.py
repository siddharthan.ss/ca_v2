from accounts.models import batch, regulation, programme

programme.objects.get_or_create(
    name="Under Graduate",
    acronym="UG",
)
programme.objects.get_or_create(
    name="Post Graduate",
    acronym="PG",
)
programme.objects.get_or_create(
    name="Doctorate of Philosophy",
    acronym="PhD",
)

regulation.objects.all().delete()
regulation_inst_old = regulation.objects.create(
    start_year=2012,
    end_year=2015,
)
regulation_inst_new = regulation.objects.create(
    start_year=2016,
    end_year=2019,
)

batch.objects.all().delete()

batch_list = (
    ('2013-06-26', 2013, 2017, 'UG'),
    ('2014-06-26', 2014, 2018, 'UG'),
    ('2015-06-26', 2015, 2019, 'UG'),
)

for entry in batch_list:
    batch.objects.get_or_create(
        start_year=entry[1],
        end_year=entry[2],
        programme=programme.objects.get(acronym=entry[3]),
        regulation=regulation_inst_old,
    )
    print('Created ' + str(entry[1]) + ' - ' + str(entry[2])+ ' - ' + str(entry[3]))

batch_list = (
    ('2015-06-26', 2016, 2020, 'UG'),
    ('2015-06-26', 2017, 2021, 'UG'),
    ('2016-06-26', 2016, 2018, 'PG'),
    ('2016-06-26', 2017, 2019, 'PG'),
    ('2016-06-26', 2016, 2018, 'PhD'),
    ('2016-06-26', 2017, 2019, 'PhD'),
)

for entry in batch_list:
    batch.objects.get_or_create(
        start_year=entry[1],
        end_year=entry[2],
        programme=programme.objects.get(acronym=entry[3]),
        regulation=regulation_inst_new,
    )
    print('Created ' + str(entry[1]) + ' - ' + str(entry[2])+ ' - ' + str(entry[3]))
