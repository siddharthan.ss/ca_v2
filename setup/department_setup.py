from accounts.models import department, department_section, programme


departments = (
    ('Computer Science and Engineering', 'CSE',17),
    ('Information Technology', 'IT',18),
    ('Mechanical Engineering', 'MECH',12),
    ('Production Engineering', 'PROD',15),
    ('Chemistry', 'CHE',-1),
    ('Physics', 'PHY',-1),
    ('Maths', 'MAT',-1),
    ('Civil', 'CIVIL',11),
    ('Electrical', 'EEE',13),
    ('Electronics', 'ECE',14),
    ('Instrumentation', 'EIE',16),
    ('bio-tech', 'IBT',19),
)

for dept in departments:
    if dept[1] == "CHE" or dept[1] == "HOSTEL" or dept[1] == "PHY" or dept[1] == "MAT":
        department.objects.get_or_create(name=dept[0], acronym=dept[1], department_code=dept[2] ,is_core=False)
    else:
        department.objects.get_or_create(name=dept[0], acronym=dept[1], department_code=dept[2], is_core=True)
    print('Created department ' + dept[1])

for dept in department.objects.filter(is_core=True):
    for prog in programme.objects.all():
        department_section_inst = department_section.objects.get_or_create(
            department = dept,
            programme = prog,
            section_name = "A"
        )
        print(str(department_section_inst) + ' Created')

        if dept.acronym == "MECH" or dept.acronym == "CIVIL":
            if prog.acronym == "UG":
                department_section_inst = department_section.objects.get_or_create(
                    department=dept,
                    programme=prog,
                    section_name="B"
                )
                print(str(department_section_inst) + ' Created')