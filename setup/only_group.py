from django.contrib.auth.models import Group

groups = (
    'principal',
    'hod',
    'programme_coordinator',
    'faculty_advisor',
    'faculty',
    'student',
    'network_admin',
    'hostel_admin',
    'alumni_staff',
    'coe_staff',
)

for group in groups:
    g = Group.objects.get_or_create(name=group)
    print('created group ' + group)