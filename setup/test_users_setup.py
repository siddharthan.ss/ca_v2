from django.utils import timezone

from accounts.models import CustomUser

"""
student.objects.all().delete()
staff.objects.all().delete()
"""
CustomUser.objects.all().delete()
#CustomUser.objects.all().get(email='gct@gct.ac.in').delete()
CustomUser.objects.create_superuser(email='gct@gct.ac.in',password='server#1234',activation_key='abcd',key_expires=timezone.now())
CustomUser.objects.filter(email='gct@gct.ac.in').update(is_verified=True)
print('Created superuser gct@gct.ac.in')
CustomUser.objects.create_user(email='stud@gct.in', password='123456',is_staff_account=False,activation_key='abcd',key_expires=timezone.now())
CustomUser.objects.filter(email='stud@gct.in').update(is_verified=True)
CustomUser.objects.filter(email='stud@gct.in').update(has_filled_data=True)
CustomUser.objects.filter(email='stud@gct.in').update(is_approved=True)
print('Created student stud@gct.in')
